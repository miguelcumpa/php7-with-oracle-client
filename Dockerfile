FROM nanoninja/php-fpm:7.2
RUN mkdir ./lib
COPY ./lib/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm ./lib/
COPY ./lib/oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm ./lib/
COPY ./lib/oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.x86_64.rpm ./lib/
RUN apt install alien libaio1 -y
RUN alien -i ./lib/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
RUN alien -i ./lib/oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm
RUN alien -i ./lib/oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.x86_64.rpm
RUN echo /usr/lib/oracle/12.2/client64/lib > /etc/ld.so.conf.d/oracle.conf
RUN ln -s /usr/bin/sqlplus64 /usr/local/bin/sqlplus
RUN ln -s /usr/include/oracle/12.2/client64 /usr/lib/oracle/12.2/client64/include
RUN echo export ORACLE_HOME=/usr/lib/oracle/12.2/client64 > /etc/profile.d/oracle.sh
RUN echo export ORACLE_HOME=/usr/lib/oracle/12.2/client64 >> /root/.bashrc
RUN export ORACLE_HOME=/usr/lib/oracle/12.2/client64
RUN ldconfig
RUN pecl install oci8
WORKDIR /var/www/html
